## Description and Overview¶
The NetCDF Operators (NCO) are a suite of file operators that facilitate
manipulation and analysis of self-describing data stored in the NetCDF or
HDF4/5 formats.

To access the NetCDF operators, load the nco module file with the 'module load
nco' command. This command will automatically load the NetCDF module file.

## Documentation

[NCO Homepage](http://nco.sourceforge.net/)

## Availability

4.6.0, 4.6.1, 4.6.2, 4.6.4, 4.6.6, 4.6.7, 4.6.9, 4.7.4(default)
