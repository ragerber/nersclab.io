## Description and Overview
NCVIEW is a visual browser for NetCDF format files.

## How to Use NCView
To use NCView, first set up the necessary X environment, then load the module:
```
module load ncview
```
To get usage instructions for ncview type:

```
ncview
```

## Documentation

[NCView Homepage](http://meteora.ucsd.edu/~pierce/ncview_home_page.html)

## Availability

2.1.6(default)

