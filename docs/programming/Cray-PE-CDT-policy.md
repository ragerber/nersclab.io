# Cray Programming Environment (CDT) Software Update Policy

## Policy

In order to provide a predictable, stable and consistent programming environment while still making necessary software updates, NERSC will be updating the Cray Programming Environment, namely the Cray Developer Toolkit (CDT), and the Intel compilers on a set cadence on Cori. CDT consists of compilers, MPI, scientific and I/O libraries, profiling and debugging tools, etc. See [monthly CDT release notes](https://pubs.cray.com/browse/xc/article/released-cray-xc-programming-environments) for full list of software in each CDT.

* New CDT software will be installed every 3 months. The CDT releases in December, March, June, and September will be installed. The actual versions may vary due to software verification and critical bug fixes. The new versions will not be made the defaults when installed.

* New software defaults will be set twice a year: once in January at the Allocation Year Rollover (the previous year's September release) and once in July (the March release). The actual versions may vary due to software verification and critical bug fixes.

* No more than four CDT versions will be available on the system at any given time. 

* Seven days advance notice will be given prior to making the changes described above, except as needed for critical security or other concerns.

### Notes

* This policy may evolve over time to better serve users' software needs.

* If you need a CDT version that we have removed from the system, please open a ticket with [NERSC Consulting](https://help.nersc.gov).

