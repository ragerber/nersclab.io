# Community File System

The Community File System will be deployed January 21, 2020. It will
replace the Project File System as a globally available file system
for sharing scientific data. Please see 
[the slides](https://www.nersc.gov/assets/Uploads/CFS-Deployment-Talk-for-NUG.pdf)
and
[the video](https://youtu.be/ftBJTDjL49I) from the December 2019 NUG
videoconference for details on the deployment.

Quotas on Community will be determined by DOE Program Managers based on
information PIs supply in their yearly ERCAP requests. Temporary quota
increases may be granted at NERSC's discretion but only until the end
of the current allocation year.

!!! note
    See [quotas](quotas.md) for detailed information about inode,
    space quotas, and file system purge policies.

