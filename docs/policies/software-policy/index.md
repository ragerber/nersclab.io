# NERSC Software Support Policy

Starting in AY2020, NERSC software support is guided by the policies shown
here. Our aims in setting these policies are to help us to offer more 
effective support, and to give better visibility of the current state and 
future plans for software packages hosted at NERSC.

The pages in this section (will) outline:

1. The goals, priorities, and intent which guides this policy.
2. Where and how we make software available to users
3. The levels of support we offer and their characteristics.
4. The processes of setting and changing the support level of a package.
5. Our use of testing to verify function and performance of supported software.
6. How we measure and report the status of NERSC-provided software

The pages will be added in coming weeks.
