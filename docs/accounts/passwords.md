# Passwords

## Password and Account Protection

A user is given a username (also known as a login name) and associated
password that permits her/him to access NERSC resources.  This
username/password pair may be used by a single individual only:
*passwords must not be shared with any other person*. Users who
share their passwords will have their access to NERSC disabled.

Passwords must be changed as soon as possible after exposure or
suspected compromise.  Exposure of passwords and suspected compromises
must immediately be reported to NERSC at security@nersc.gov or the
Account Support Group, accounts@nersc.gov.

## Forgotten Passwords

If you forget your password or if it has recently expired, you can
reset your password by clicking the 'Reset your password' link on
the [Iris login page](https://iris.nersc.gov).  Enter your username
and, if MFA has been enabled, a MFA OTP, and click OK. NERSC will
send you a secret code. The secret code will be valid for 24 hours.
In the [password resetting page](https://iris.nersc.gov/reset-password),
enter the registered email address, the secret code, and your new
password. Your password should meet the password policy explained
there and must be strong according to the provided password strength
meter.

If you still have a problem after trying that, you will need to
call Operations at 800-666-3772, menu option 1, or 510-486-6821 to
have your password reset. Then, you will receive a secret code by
email. With the secret code, follow the steps above.

## Passwords for New Users

NERSC must have a NERSC Appropriate Use Policy (AUP) form on file
before activating a user's account and assigning a user a password
in the Iris system.  This form can be submitted online.

Once we have received the form and attached it to your account
(assuming that your PI has already requested that you be added to
their project repository), we will send you a secret code via email.
The secret code will be valid for 24 hours.  In the [password
resetting page](https://iris.nersc.gov/reset-password), enter the
registered email address, the secret code, and your new password.
Your password should meet the password policy explained there and
must be strong according to the provided password strength meter.

## How To Change Your Password in Iris

All of NERSC's computational systems are managed by the LDAP protocol
and use the Iris password. Passwords cannot be changed directly on
the computational machines, but rather the Iris password itself
must be changed:

1.  Point your browser to [https://iris.nersc.gov](https://iris.nersc.gov).
2.  Click the Iris icon in the top left corner.
3.  Select the 'Details' tab.
4.  Click on the 'Reset password' box in the 'SElf-service User
    Info' section.
5.  NERSC will send you a secret code via email. The secret code
    will be valid for 24 hours.
6.  In the [password resetting
    page](https://iris.nersc.gov/reset-password), enter the registered
    email address, the secret code, and your new password. Your
    password should meet the password policy explained there and
    must be strong according to the provided password strength
    meter.

Passwords must be changed under any one of the following circumstances:

*  At least every six months.
*  Immediately after someone else has obtained your password (do *NOT*
   give your password to anyone else).
*  As soon as possible, but at least within one business day after a
   password has been compromised or after you suspect that a password
   has been compromised.
*  On direction from NERSC staff.

Your new password must adhere to NERSC's password requirements.

## Password Requirements

As a Department of Energy facility, NERSC is required to adhere to
Department of Energy guidelines regarding passwords.  The following
requirements conform to the Department of Energy guidelines regarding
passwords, namely DOE Order 205.3 and to Lawrence Berkeley National
Laboratory's [RPM §9.02 Operational Procedures for Computing and
Communications](https://commons.lbl.gov/display/rpm2/Security+for+Information+Technology#SecurityforInformationTechnology-64452698).

When users are selecting their own passwords for use at NERSC, the
following requirements must be used.

*  Passwords must contain at least eight nonblank characters.
*  Passwords must contain a combination of upper and lowercase
   letters, numbers, and at least one special character within the
   first seven positions.
*  Passwords must contain a nonnumeric letter or symbol in the first
   and last positions.
*  Passwords must not contain the user login name.
*  Passwords must not include the user's own or (to the best of his or
   her knowledge) a close friend's or relative's name, employee
   number, Social Security or other Identification number, birth date,
   telephone number, or any information about him or her that the user
   believes could be readily learned or guessed.
*  Passwords must not (to the best of the user's knowledge) include
   common words from an English dictionary or a dictionary of another
   language with which the user has familiarity.
*  Passwords must not (to the best of the user's knowledge) contain
   commonly used proper names, including the name of any fictional
   character or place.
*  Passwords must not contain any simple pattern of letters or
   numbers such as "qwertyxx".

## Login Failures

Your login privileges will be disabled if you have five login failures
while entering your password on a NERSC machine.  You do not need a
new password in this situation.  You can clear your login failures on
all systems by simply logging in to Iris.  No additional actions are
necessary.
