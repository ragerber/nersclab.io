NERSC provides its users with the means to store, manage, and share
their research data products.

In addition to systems specifically tailored for data-intensive
computations, we provide a variety of storage resources optimized for
different phases of the data lifecycle; tools to enable users to
manage, protect, and control their data; high-speed networks for
intra-site and inter-site (ESnet) data transfer; gateways and portals
for publishing data for broad consumption; and consulting services to
help users craft efficient data management processes for their
projects.
